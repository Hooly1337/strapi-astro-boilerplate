### Run dev strapi

`
    cd strapi && docker-compose up
`

### Run dev astro

`
cd frontend && yarn && yarn run dev
`