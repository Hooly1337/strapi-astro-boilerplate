import React, { useState } from "react";


export function Header() {
    const [counter, setCount] = useState(0);

    return (
        <div onClick={() => setCount(counter+1)}>{counter}</div>
    )
}
